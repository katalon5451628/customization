import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration

WebUI.click(findTestObject('Install_theme/Page_welcome  API Developer Portal/a_Manage'))

WebUI.click(findTestObject('Install_theme/Page_welcome  API Developer Portal/a_Appearance'))

WebUI.click(findTestObject('Install_theme/Page_Appearance  API Developer Portal/a_Settings'))

WebUI.click(findTestObject('Install_theme/Page_nic_theme  API Developer Portal/a_Favicon(active tab)'))

WebUI.uncheck(findTestObject('Install_theme/Page_nic_theme  API Developer Portal/Uncheckfavicon'))

TestObject fileInput = findTestObject('Install_theme/Page_nic_theme  API Developer Portal/input_Upload favicon image_filesfavicon_upload')

// Specify the path to the zip file
String filePath = 'D://Manish Purwar//NIC//NIC//Software Quality//Katalon_Studio//workspace//Developer_Customization_Portal//Data Files//nic_theme//favicon.ico'

// Upload the file
WebUI.uploadFile(fileInput, filePath)


WebUI.click(findTestObject('Install_theme/Page_nic_theme  API Developer Portal/a_Logo image'))

WebUI.uncheck(findTestObject('Install_theme/Page_nic_theme  API Developer Portal/input_Logo image_default_logo'))

TestObject fileInput = findTestObject('Install_theme/Page_nic_theme  API Developer Portal/input_Upload logo image_fileslogo_upload')

// Specify the path to the zip file
String filePath = 'D://Manish Purwar//NIC//NIC//Software Quality//Katalon_Studio//workspace//Developer_Customization_Portal//Data Files//nic_theme//logo.png'

// Upload the file
WebUI.uploadFile(fileInput, filePath)



String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath = projectDir + File.separator + 'Screenshot' + File.separator + 'logo.png'
WebUI.takeScreenshot(relativeFilePath)
WebUI.click(findTestObject('Install_theme/Page_nic_theme  API Developer Portal/Save Configuration'))


String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath = projectDir + File.separator + 'Screenshot' + File.separator + 'themecomplete.png'
WebUI.takeScreenshot(relativeFilePath)