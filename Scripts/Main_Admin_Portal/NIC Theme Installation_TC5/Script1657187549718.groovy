import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.click(findTestObject('Install_theme/Page_welcome  API Developer Portal/a_Manage'))

WebUI.mouseOver(findTestObject('Install_theme/Page_welcome  API Developer Portal/a_Appearance'))

WebUI.click(findTestObject('Install_theme/Page_welcome  API Developer Portal/a_Install new theme'))

//WebUI.click(findTestObject('Install_theme/Page_welcome  API Developer Portal/a_Install new theme'))
// Locate the file input element on the page using a Test Object
TestObject fileInput = findTestObject('Install_theme/Page_Add new theme  API Developer Portal/Upload_theme')

// Specify the path to the zip file
String filePath = 'D://Manish Purwar//NIC//NIC//Software Quality//Katalon_Studio//workspace//Developer_Customization_Portal//Theme//nic_theme.zip'

// Upload the file
WebUI.uploadFile(fileInput, filePath)

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath9 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'theme_Upload.png'

WebUI.takeScreenshot(relativeFilePath9)

WebUI.click(findTestObject('Install_theme/Page_Add new theme  API Developer Portal/Countinue'))

WebUI.click(findTestObject('Install_theme/Page_welcome  API Developer Portal/a_Enable newly added themes'))

WebUI.click(findTestObject('Install_theme/Page_Appearance  API Developer Portal/a_Enable and set as default'))

WebUI.callTestCase(findTestCase('Main_Admin_Portal/Change logo_TC7'), [:], FailureHandling.STOP_ON_FAILURE)

