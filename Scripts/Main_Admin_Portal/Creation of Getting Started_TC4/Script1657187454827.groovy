import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.callTestCase(findTestCase('Basic_Admin_Portal/Dashboard_TC11'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GettingStarted/Page_welcome  API Developer Portal/a_Custom block library'))

WebUI.setText(findTestObject('GettingStarted/Page_Custom block library  API Developer Portal/input_Block description_info'), 
    'Getting Started')

WebUI.sendKeys(findTestObject('GettingStarted/Page_Custom block library  API Developer Portal/input_Block description_info'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('GettingStarted/Page_Custom block library  API Developer Portal/a_Edit'))

WebUI.click(findTestObject('GettingStarted/Page_Edit Basic block Getting Started en  API Developer Portal/button_Source'))

WebUI.click(findTestObject('GettingStarted/Page_Edit Basic block Getting Started en  API Developer Portal/textarea'))

WebUI.sendKeys(findTestObject('GettingStarted/Page_Edit Basic block Getting Started en  API Developer Portal/textarea'), 
    Keys.chord(Keys.CONTROL, 'a'))

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath6 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'Started_Basic block.png'

WebUI.takeScreenshot(relativeFilePath6)

// Define the file path
def filePath = 'D://Manish Purwar//NIC//NIC//Software Quality//Katalon_Studio//workspace//Developer_Customization_Portal//Plugins//gettingstarted.txt'

// Create a File object
def file = new File(filePath)

// Use Scanner to read the file content
def scanner = new Scanner(file)

// Read the content of the file into a string
def fileContent = ''

while (scanner.hasNextLine()) {
    def line = scanner.nextLine()

    fileContent += (line + '\n')
}

// Close the scanner
scanner.close()

// Set the text value of the object element
WebUI.setText(findTestObject('GettingStarted/Page_Edit Basic block Getting Started en  API Developer Portal/textarea'), 
    fileContent)

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath7 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'Started_block_Update.png'

WebUI.takeScreenshot(relativeFilePath7)

WebUI.click(findTestObject('GettingStarted/Page_Edit Basic block Getting Started en  API Developer Portal/save'))

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath8 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'Getting_Started.png'

WebUI.takeScreenshot(relativeFilePath8)

