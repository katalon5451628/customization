import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.comment('Welcome to Custome block library')

WebUI.callTestCase(findTestCase('Basic_Admin_Portal/Dashboard_TC11'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Welcome/Page_Custom block library  API Developer Portal/a_Custom block library'))

WebUI.setText(findTestObject('Welcome/Page_Custom block library  API Developer Portal/input_Block description_info'), 'Welcome Banner')

WebUI.sendKeys(findTestObject('Welcome/Page_Custom block library  API Developer Portal/input_Block description_info'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Welcome/Page_Custom block library  API Developer Portal/a_Edit'))

WebUI.click(findTestObject('Welcome/Page_Edit Basic block Welcome Banner en  API Developer Portal/button_Source'))

WebUI.click(findTestObject('Welcome/Page_Edit Basic block Welcome Banner en  API Developer Portal/textarea'))

WebUI.sendKeys(findTestObject('Welcome/Page_Edit Basic block Welcome Banner en  API Developer Portal/textarea'), Keys.chord(
        Keys.CONTROL, 'a'))

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath3 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'Welcome_Basic block.png'

WebUI.takeScreenshot(relativeFilePath3)

// Define the file path
def filePath = 'D://Manish Purwar//NIC//NIC//Software Quality//Katalon_Studio//workspace//Developer_Customization_Portal//Plugins//welcomebanner.txt'

// Create a File object
def file = new File(filePath)

// Use Scanner to read the file content
def scanner = new Scanner(file)

// Read the content of the file into a string
def fileContent = ''

while (scanner.hasNextLine()) {
    def line = scanner.nextLine()

    fileContent += (line + '\n')
}

// Close the scanner
scanner.close()

// Set the text value of the object element
WebUI.setText(findTestObject('Welcome/Page_Edit Basic block Welcome Banner en  API Developer Portal/textarea'), fileContent)

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath4 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'Welcome_block_Update.png'

WebUI.takeScreenshot(relativeFilePath4)

WebUI.click(findTestObject('Welcome/Page_Edit Basic block Welcome Banner en  API Developer Portal/input_The log entry explaining the changes in this revision_op'))

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath5 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'Welcome_Banner.png'

WebUI.takeScreenshot(relativeFilePath5)

