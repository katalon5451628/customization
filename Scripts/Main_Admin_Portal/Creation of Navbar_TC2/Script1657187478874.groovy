import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.io.File as File
import java.io.IOException as IOException
import java.util.Scanner as Scanner
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.callTestCase(findTestCase('Basic_Admin_Portal/Dashboard_TC11'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Navbar/Page_Structure  API Developer Portal/a_Add custom block'))

WebUI.setText(findTestObject('Object Repository/Navbar/Page_Add custom block  API Developer Portal/input_Block description_info0value'), 
    'Navbar')

WebUI.click(findTestObject('Navbar/Page_Add custom block  API Developer Portal/button_Source'))

WebUI.click(findTestObject('Navbar/Page_Add custom block  API Developer Portal/textarea'))

// Define the file path
def filePath = 'D://Manish Purwar//NIC//NIC//Software Quality//Katalon_Studio//workspace//Developer_Customization_Portal//Plugins//navbar.txt'

// Create a File object
def file = new File(filePath)

// Use Scanner to read the file content
def scanner = new Scanner(file)

// Read the content of the file into a string
def fileContent = ''

while (scanner.hasNextLine()) {
    def line = scanner.nextLine()

    fileContent += (line + '\n')
}

// Close the scanner
scanner.close()

// Set the text value of the object element
WebUI.setText(findTestObject('Navbar/Page_Add custom block  API Developer Portal/textarea'), fileContent)

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath1 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'NavbarPage.png'

WebUI.takeScreenshot(relativeFilePath1)

WebUI.click(findTestObject('Navbar/Page_Add custom block  API Developer Portal/input_The log entry'))

WebUI.click(findTestObject('Navbar/Page_Configure block  API Developer Portal/input_Browse available tokens_settingslabel_display'))

WebUI.click(findTestObject('Navbar/Page_Configure block  API Developer Portal/Region'))

WebUI.sendKeys(findTestObject('Navbar/Page_Configure block  API Developer Portal/Region'), 'Disabled', FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Navbar/Page_Configure block  API Developer Portal/save block'))

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath2 = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'NavbarSave.png'

WebUI.takeScreenshot(relativeFilePath2)

