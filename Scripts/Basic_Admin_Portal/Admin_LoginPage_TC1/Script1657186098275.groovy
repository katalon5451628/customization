import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.click(findTestObject('AdminLoginPage/Page_welcome  API Developer Portal/a_Sign in'))

WebUI.setText(findTestObject('Object Repository/AdminLoginPage/Page_Sign in  API Developer Portal/input_Username_name'), 
    GlobalVariable.username)

WebUI.setText(findTestObject('Object Repository/AdminLoginPage/Page_Sign in  API Developer Portal/input_Password_pass'), 
    GlobalVariable.password)

WebUI.click(findTestObject('AdminLoginPage/Page_Sign in  API Developer Portal/button_Sign in'))

String projectDir = RunConfiguration.getProjectDir()

// Construct a relative path for your file or screenshot
String relativeFilePath = (((projectDir + File.separator) + 'Screenshot') + File.separator) + 'loginform.png'

WebUI.takeScreenshot(relativeFilePath)

