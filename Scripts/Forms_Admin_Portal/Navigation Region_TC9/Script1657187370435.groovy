import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.comment('Navigation Region')

WebUI.click(findTestObject('Navbar/Page_welcome  API Developer Portal/a_Manage'))

WebUI.mouseOver(findTestObject('Navbar/Page_welcome  API Developer Portal/a_Structure'))

WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/a_Block layout'))

WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/a_Place block in the Navigation region'))

WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/a_Place block'))

WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/label_Display title'))

//WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/select_Navigation'))
//WebUI.sendKeys(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/select_Navigation'), 'Navigation')
WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/button_Save block'))

