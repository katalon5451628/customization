import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.click(findTestObject('Navbar/Page_welcome  API Developer Portal/a_Manage'))

WebUI.mouseOver(findTestObject('Navbar/Page_welcome  API Developer Portal/a_Structure'))

WebUI.click(findTestObject('Navbar/Page_Structure  API Developer Portal/a_Block layout'))

WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/select_Site'))

WebUI.sendKeys(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/select_Site'), 'Top Bar')

WebUI.sendKeys(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/select_Site'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/select_Main'))

WebUI.sendKeys(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/select_Main'), 'Top Bar')

WebUI.click(findTestObject('Navigation Bar/Page_Block layout  API Developer Portal/Saveblock'))

